import os
import paramiko

def check_ssh_connection(ip_address, username, password, port=22):
    try:
        # Create an SSH client
        ssh_client = paramiko.SSHClient()

        # Automatically add the server's host key
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # Connect to the server
        ssh_client.connect(ip_address, port=port, username=username, password=password, timeout=10)

        print(f"Connected to {ip_address} via SSH successfully.")

        return ssh_client

    except Exception as e:
        print(f"Error: {e}")
        return None

def copy_files_to_ssh(local_folder, ssh_client, file_extension, remote_folder):
    try:
        # Create an SFTP session from the SSH client
        sftp = ssh_client.open_sftp()

        # List local files with the specified extension
        local_files = [f for f in os.listdir(local_folder) if f.endswith(file_extension)]

        # Copy each file to the SSH server
        for local_file in local_files:
            local_path = os.path.join(local_folder, local_file)
            remote_path = os.path.join(remote_folder, local_file)
            sftp.put(local_path, remote_path)
            print(f"File '{local_file}' copied from {local_folder} to SSH client:{remote_folder}")

        # Close the SFTP session
        sftp.close()

    except Exception as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    # Replace these with your actual values
    target_ip = "192.168.0.200"
    ssh_username = "root"
    ssh_password = "deltatau"

    local_folder_path = "./PowerPMAC2/Bin/Debug"
    remote_folder_path = "/var/ftp/usrflash/Project/Bin/Debug"

    ssh_client = check_ssh_connection(target_ip, ssh_username, ssh_password)

    if ssh_client:
        # 1.a) Copy CAPP files 
        copy_files_to_ssh(local_folder="./PowerPMAC2/Bin/Debug", ssh_client = ssh_client, file_extension=".out", remote_folder="/var/ftp/usrflash/Project/Bin/Debug")
        
        # 1.b) Copy CPLC and Realtime Routine files SKIPPED

        # 2. Download the Script Programs
        copy_files_to_ssh(local_folder="./PowerPMAC2/PMAC Script Language/Global Includes", ssh_client = ssh_client, file_extension=".pmh", remote_folder="/var/ftp/usrflash/Project/PMAC Script Language/Global Includes")
        copy_files_to_ssh(local_folder="./PowerPMAC2/PMAC Script Language/Libraries", ssh_client = ssh_client, file_extension=".pmc", remote_folder="/var/ftp/usrflash/Project/PMAC Script Language/Libraries")
        copy_files_to_ssh(local_folder="./PowerPMAC2/PMAC Script Language/Motion Programs", ssh_client = ssh_client, file_extension=".pmc", remote_folder="/var/ftp/usrflash/Project/PMAC Script Language/Motion Programs")
        copy_files_to_ssh(local_folder="./PowerPMAC2/PMAC Script Language/PLC Programs", ssh_client = ssh_client, file_extension=".pmc", remote_folder="/var/ftp/usrflash/Project/PMAC Script Language/PLC Programs")

        print("File Transfer complete")

        # Close the SSH connection after file transfer
        ssh_client.close()
    else:
        print("Failed to establish SSH connection.")
