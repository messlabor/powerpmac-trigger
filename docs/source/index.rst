.. PMAC Project for Helmholtz Coil documentation master file, created by
   sphinx-quickstart on Sat Jan  6 14:22:27 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Power PMAC Project for DMM Triggering!
===========================================================

This document was written during working on the Power PMAC project for a Helmholtz Coil experiment, where defined rotation steps in the motor must trigger measurements on a multimeter. This project used the Omron CK3E Motion Controller with Beckhoff I/O (EL1008 and EL2004). Communication to the computer is done vie Ethernet, while devices communicate via EtherCAT.

.. toctree::
   usage
   plcs
   EtherCAT

Overview
-------------

Follow the steps in the :ref:`usage` section to set up Power PMAC IDE. Below is the project-tree with some important files highlighted (you might want to check them to get used to the project).

.. image:: images/project-tree-overview.PNG

* CPU System contain important settings regarding the Clock Settings.
* Motor 1 will open the settings diagram that helps check motor configurations.
* CoordinateSystem1 contain the axis definitions. Crucial for PMAC projects.
* pp_startup.txt contain commands to be run on startup. In our case the EtherCAT Initialization PLC.
* globaldefinitions.pmh contain our user defined variables used in the PLCs and also in the IOC Project.
* At the end, the two PLCs are discussed with more detail at the :ref:`plcs` section.
