PLCs
=========

The user defined variables are written in the global definitions.pmh file. These variables are used for the triggering plc, thus one must first take a look at what they are.

The variables below are used in the EPICS IOC. For more information, please refer to the `EPICS IOC PMAC Trigger <https://codebase.helmholtz.cloud/messlabor/ioc-pmac-trigger>`_ repository.

.. literalinclude:: ../../PowerPMAC2/PMAC Script Language/Global Includes/global definitions.pmh
    :language: C

Triggering
-------------

The triggering system was written specifically for the Multimeter HP3458A which is triggered on "falling edge" between 5 to 0 Volts. 

.. figure:: /images/scheme-trigger.png
    :alt: Triggering System
    :align: center

The project involves controlling a Keysight 3458A Digital Multimeter by sending trigger pulses using a state machine implemented in the trigger.plc code. The state machine manages the process of generating trigger pulses based on various parameters such as step size, total number of measurements, and modulation.

#. **INIT (State 0):** This state initializes the system, resetting various values and preparing for operation. It resets the BatchPos array, which stores measurement results. Resets all relevant variables and flags. Waits for one second before transitioning to the IDLE state.
#. **IDLE (State 1):** In this state, the system is waiting for a command to start measurements (`cmd_run`). It also listens for a reset command (`reset_all`), which would transition it back to the INIT state.
#. **START (State 2):** Prepares the system for measurement. Sets up measurement variables and parameters. Checks conditions for starting measurements based on trigger frequency and total number of measurements. Transitions to the MEASUREMENT state if conditions are met, otherwise to the ABORT state.
#. **ABORT (State 3):** This state handles the aborting of the measurement process. It checks if the `cmd_run` flag is reset, indicating the measurement process should stop. Transitions back to the IDLE state if the measurement is aborted.
#. **MEASUREMENT (State 4):** This is the main state where measurements are taken. Calculates the modulation and performs actions based on its value. Handles the trigger output (TriggerOutput) based on the modulation. Continues measurements until the total count is reached or until aborted. Transitions to the DONE state once measurements are completed.
#. **DONE (State 5):** Marks the completion of measurements. Waits for the user-defined signal (`measurement_done`) to transition back to the IDLE state. Can also transition back to the INIT state if a reset command is received (`reset_all`).

Additional Notes:

- The code employs local variables to store temporary values used within each state.
- Various flags (`cmd_run`, `reset_all`, `measurement_done`, `cmd_abort`) control the flow of the state machine.
- The `TriggerOutput` controls the actual triggering of the digital multimeter.


.. literalinclude:: ../../PowerPMAC2/PMAC Script Language/PLC Programs/trigger.plc
    :language: C



ECAT ini
-------------

The plc below is responsible for activating etherCAT on startup, which means that if the PMAC device is reset, EtherCAT connection is automatically established. Without this, one would need to do it manually every restart.

.. literalinclude:: ../../PowerPMAC2/PMAC Script Language/PLC Programs/ecat.plc
    :language: C

    