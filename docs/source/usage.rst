Usage
===========

In general, this project only requires one to download and save it inside the PMAC device, in our case the Omron CK3E. The project, programs were let as simple as possible to avoid having to use th Power PMAC IDE very often. The idea is to control most of the features with EPICS,  see project `IOC PMAC TRIGGER <https://codebase.helmholtz.cloud/messlabor/ioc-pmac-trigger.git>`_.

The Power PMAC IDE, at the moment, does not run on Linux OS, thus we try to customize the project only with essentials, and leave the rest of the automations to epics and python.

We present in this section two methods of Downloading and saving the project inside the PMAC device

Power PMAC IDE
------------------

First thing to do is to make sure that your PMAC device has the correct project with the hardware correctly set. If anything changes in the hardware.

#. Install the Power PMAC IDE `here <https://automation.omron.com/en/ca/products/family/PMAC%20IDE>`_
#. Download the project from the gitlab
#. In the communication setup window you can probably use the stardand IP and credentials. 
    * IP Address: 192.168.0.200
    * User: root
    * Password: deltatau

    .. image:: images/communication-setup.PNG


    .. note:: 
        
        Your Network Settings (Ethernet) must be set correctly, see example below:
        
        .. image:: images/local-ip-settings.PNG

#. In the Solution Explorer, right-click on the PowerPMAC2 project and click "Build and Download Program"

    .. image:: images/project-tree-usage.PNG

    .. note::

        Build and Download only saves the programs in the RAM, which means after turning off the PMAc device, your project will not be there. To save it in the flash memory, issue the command "save" in the terminal inside the IDE. 

#. If no errors were shown so far, we are safe to assume the project is at the Device. 

Linux Command
------------------

One can use ssh to communicate with the PMAC device without using the IDE. For that:

#. Open a prompt command in your local machine, if the ip settings are correct, and run 

    .. code-block:: bash

        ssh root@192.168.0.200

    use the password "deltatau"

#. run the command gpascii to open the ASCII interface. From that one can use Online Commands from the Power PMAC, such as $$$, $$$***, save, jog, ...

.. note::

    The plan would be to build, download and save the project in the PMAC device without the need of Windows OS or the Power PMAC IDE. This should be possible using the ssh connection to the pmac.
