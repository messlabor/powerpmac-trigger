EtherCAT Clock Settings
===========================================================

1. Start with $$$*** then SAVE and issua $$$.
2. Opena a new project and select the template for ethercat.
   
    .. figure:: /images/newproject.png
        :align: center

3. Go to the System -> CPU -> Clock Settings node from the project viewer.

    .. figure:: /images/cpu-settings.png
        :align: center

4. Make your selection for the CPU rate.

    .. figure:: /images/clock-settings.png
        :align: center

5. Select the ACCEPT button.
6. In the terminal window ente SAVE then POWER CYCLE the PMAC.
7. When PMAC is back up, enter "ecat reset" in the terminal window.
8. In project viewer right click on master and select SCAN ETHERCAT NETWORK.
9.  Double click on Master0 Node, go to the tab Master and make sure the cycle time is correct. Then go to the DC clock table and select MASTER SHIFT mode.
10. Double click each slave and on the DC Clock page set the shift time to 120 usec (at 4kHz).

    .. figure:: /images/bus-shift-settings.png
        :align: center
    
11. Right click on master and select LOAD MAPPING TO POWER PMAC.
12. Enter "ecat reset" in the terminal window.
13. Activate Ecat.
